//Reduce 

const checkParentheses = (parentheses) => {
  const parens = parentheses.split('').reduce((acc, val) => {
    if(acc < 0){
      return acc;
    }
    else if(val === '(') {
      return ++acc;
    } else if (val === ')') {
      return --acc;
    }
    return acc;
  }, 0);
  return parens === 0;
}

// Map

// const checkParentheses = (parentheses) => {
//   let sum = 0;

//   parentheses.split('').map(el => {
//     if(sum < 0) {
//       return sum
//     } else if(el === '('){
//       return sum++
//     } else if (el === ')'){
//       return sum--
//     } else {
//       return sum;
//     }
    
//   })
//   return sum === 0;
// }




console.log( checkParentheses('()()()()')); // true
console.log( checkParentheses('(((())))')); // true
console.log( checkParentheses('))()))')); // false

console.log( checkParentheses('(())()()')); //true
console.log( checkParentheses('()(()')); //false
console.log( checkParentheses(')(')); //false