const shoppingList = [
  { recipe: 'muffins',
    ingredients: [
      { item: 'flour', price: 50 },
      { item: 'sugar', price: 60 },
      { item: 'milk', price: 90 },
      { item: 'eggs', price: 150 }
    ]
  },
  {
  recipe: 'salad',
    ingredients: [
      { item: 'tomatoes', price: 200 },
      { item: 'cheese', price: 160 },
      { item: 'dressing', price: 220}
    ]
  }
 ];



// let totalPrice = shoppingList.map( ({ingredients}) => ingredients.reduce((acc,val) => {
//   return acc + val.price
// }, 0)).reduce((x, y) => x + y, 0);

// console.log(totalPrice);



let totalPrice = shoppingList
  .map(ing => ing.ingredients
    .reduce((acc,val) => {
      return acc + val.price
    }, 0))
     .reduce((a, v) => a + v, 0);

console.log(totalPrice);
