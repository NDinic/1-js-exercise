function func1(a) {
  return function func2(b) {
    return function func3(c) {
      return a/b/c;
    }
  }
}

console.log(func1(1000)(100)(10));