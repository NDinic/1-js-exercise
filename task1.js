const wordsArray = ['car', '3000', 'xyz', '45', 'banana', '100', 'apple', '0.05',
'bee', '401'];
// ["apple", "banana", "bee", "car", "xyz", "0.05", "45", "100", "401", "3000"]

wordsArray.sort();
let numbers = [];
let stringsArray = [];
let sortedArray = [];

function hasNumbers(num){
  return /\d/.test(num);
}

// function getNumbers() {
//   for(let i = 0; i < wordsArray.length; i++) {
//     if(hasNumbers(wordsArray[i])){
//       numbers.push(wordsArray[i]);
//     } else {
//       stringsArray.push(wordsArray[i])
//     }
//   }
//   numbers.sort(function(a, b){
//     return a-b;
//   });
//   sortedArray = stringsArray.concat(numbers);
//   return sortedArray;
// }




function getNumbers(arr) {
  let stringArray = [];
  let numArray = [];
  // arr.forEach(el => hasNumbers(el) ? numArray.push(el) : stringArray.push(el))
  arr.map(el => hasNumbers(el) ? numArray.push(el) : stringArray.push(el))
  stringArray.sort();
  numArray.sort((a,b) => a - b);

  return [...stringArray, ...numArray]
}
console.log(getNumbers(wordsArray));